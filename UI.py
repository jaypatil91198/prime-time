import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.figure_factory as ff
import pandas as pd
import numpy as np
import dash_table
import pickle
import matplotlib.pyplot as plt
import scipy.stats as stats
from plotly.tools import mpl_to_plotly

colors = {
    'background': '#111111',
    'text': '#7FDBFF',
    'diff' : '#6E6E6E'
}

file_name = 'trivia_output2.pickle'
with open(file_name, 'rb') as handle:
    result_dict = pickle.load(handle)

conflict_trivia_df = result_dict['conflict_trivia_df']
conflict_trivia_evidence_df = result_dict['conflict_trivia_evidence_df']
comparison_trivia_df = result_dict['comparison_trivia_df']
comparison_trivia_evidence_df = result_dict['comparison_trivia_evidence_df']

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(
    children = [
        html.H1( 
            className="row",
            style = {'textAlign': 'center','font-size': '30px'},
            children = [
                html.Div(children= 'PRIME TIME')
            ]
        ),
        html.Div(
            className="row",
            children = [
                html.Div(
                    className="six columns",
                    style = {'display': 'inline-block'},
                    children = [
                        dcc.RadioItems(
                            className="row",
                            # style = {'display': 'block'},
                            id = 'radio-button',
                            labelStyle = {'display': 'inline-block','textAlign': 'center'},
                            options=[{'label': i, 'value': i} for i in ['Conflict', 'Comparison']],
                            style = {'textAlign': 'center','font-size': '20px'},
                            value = 'Conflict',
                        ),
                        html.Div(
                            className="row",
                            # style = {'display': 'inline-block'},
                            # children= 'Trivia List',
                            # style = {'font-size': '22px'}
				        ),
                        dash_table.DataTable(
                            # className="row",
                            id = 'table',
                            style_cell={'textAlign': 'left','whiteSpace': 'normal'},
                            style_table={
                                'height': '600px',
                                # 'overflowY': 'scroll',
                                # 'overflowX': 'scroll',
                                # 'border': 'thin lightgrey solid'
                            },
                            columns=[{"name": i, "id": i} for i in comparison_trivia_df.columns],
                            data = [{}]
                        )
                    ],
                    
                ),
			    html.Div(
                    className="six columns" ,
                    style = {'display': 'inline-block'},
                    children = [
                        html.Div(
                            className="row",
                            # style = {'display': 'inline-block'},
                            # children= 'Trivia List',
				        ),
                        html.Div(
                            # style = {'display': 'inline-block'},
                            className="row",
                            # children= 'Overlapped Distribution Graphs',
                            style = {'textAlign': 'center','font-size': '22px'}
				        ),
                        html.Div(
                            # style = {'display': 'inline-block','height' : '30vh'},
                            className="row",
                            # style = { 'height' : '30vh', 'width': '30vh'},
                            id='graph'
                        ),
                        html.Div(
                            # style = {'display': 'inline-block'},
                            className="row",
                            # children= "Insight's Statics",
                            style = {'textAlign': 'center','font-size': '22px'}
				        ),
                        html.Div(
                            className="row",
                            # style = {'display': 'inline-block'},
                            # children= 'Trivia List',
				        ),
                        html.Div(
                            # style = {'height' :'20vh'},
                            className="row",
                            style = {'textAlign': 'center','padding-left':'25%', 'padding-right':'1%','height' :'10vh'},
                            id='stats'
                        )
		            ] 
                )
            ] 
        )
    ]
)

@app.callback(
    [Output('table','data'),
     Output('graph','children'),
     Output('stats','children')],
    [Input('radio-button', 'value'),
     Input('table', 'selected_row_ids'),
     Input('table', 'active_cell')]
)

def update_list_graph_table(value, rows, selected_row_indices):
    print('value = ', value)
    if value == 'Conflict':
        trivia_df = pd.DataFrame(conflict_trivia_df[["Trivia"]])
        evidence_df = pd.DataFrame(conflict_trivia_evidence_df[['Stats','Overl_dist_graph']])
    elif value == 'Comparison':
        trivia_df = pd.DataFrame(comparison_trivia_df[["Trivia"]])
        evidence_df = pd.DataFrame(comparison_trivia_evidence_df[['Stats','Overl_dist_graph']])
    data = trivia_df.to_dict('records')

    if selected_row_indices is None:
        r = 0
    else:
        r = selected_row_indices['row']
    print('selected row = ', r)

    subgroup_values_list = evidence_df.iloc[r]['Overl_dist_graph']
    subgroup1 = subgroup_values_list[0]
    subgroup2 = subgroup_values_list[1]

    subgroup_stats_list = evidence_df.iloc[r]['Stats']
    stat1_dict = subgroup_stats_list[0]
    stat2_dict = subgroup_stats_list[1]

    col_list = ['Stats','Subgroup 1' , 'Subgroup 2']
    col_value_list = ['count' , 'mean' , 'median' , 'var' , 'std']
    stat1_dict_values = [value for value in stat1_dict.values()]
    stat2_dict_values = [value for value in stat2_dict.values()]
    values_list = [col_value_list, stat1_dict_values, stat2_dict_values]

    subgroup1_mu = np.mean(subgroup1)
    subgroup1_sigma = np.std(subgroup1)
    new_subgroup1 = np.linspace(subgroup1_mu - 3*subgroup1_mu, subgroup1_mu + 3*subgroup1_sigma, 100)
    subgroup2_mu = np.mean(subgroup2)
    subgroup2_sigma = np.std(subgroup2)
    new_subgroup2 = np.linspace(subgroup2_mu - 3*subgroup2_mu, subgroup2_mu + 3*subgroup2_sigma, 100)
    hist_data = [new_subgroup1, new_subgroup2]
    # hist_data = [new_subgroup1, stats.norm.pdf(new_subgroup1, subgroup1_mu, subgroup1_sigma)]
    group_labels = ['Group 1', 'Group 2']
    # plt.plot(new_subgroup1, stats.norm.pdf(new_subgroup1, subgroup1_mu, subgroup1_sigma))
    # mpl_fig = plt.figure()
    # fig = mpl_to_plotly(mpl_fig)
    # fig.update_layout(title_text='Curve Plot')
    fig = ff.create_distplot(hist_data, group_labels, bin_size=.5, show_hist=False, show_rug=False, colors=['#333F44', '#37AA9C'])
    fig.update_layout(title_text='Curve Plot',width=700, height=400)


    return [
        data,
        dcc.Graph(
            id='column',
            figure=fig
        ),
        html.Table(
            [ #Header
                html.Tr(
                    [html.Th(col) for col in col_list],
                )
            ] +
            [ #Body
                html.Tr(
                    [html.Td((values_list[j][i])) for j in range(3)]
                ) for i in range(4)
            ],
        )
    ]


if __name__ == '__main__':
    app.run_server(debug=True)
